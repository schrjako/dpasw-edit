##!/bin/sh

title=$(grep "<h3>" orig_html/$1.html | sed 's|^.*<h3>\([^<]\+\)</h3>.*$|\1|')

echo "Got chapter title: [$title]"
echo $title > chapters/$1_orig.html
echo >> chapters/$1_orig.html

grep -E "<p( |>)" orig_html/$1.html |
	sed 's|</p>|</p>\n|g' |
	grep -E "<p( |>)" |
	sed 's|^.*<p\([^>]*\)>\(.\+\)</p>.*$|<p\1>\2</p>|' |
	cat >> chapters/$1_orig.html

./insert_chars.sh chapters/$1_orig.html
sed 's/align="center"/style="text-align: center"/g' -i chapters/$1_orig.html

if [ -f "chapters/$1_edit.html" ]
then
	echo chapter edit file already exists, skipping chapters/$1_edit.html
else
	rm -f log/$1.log
	./orientate_quotation_marks chapters/$1_orig.html chapters/$1_edit.html log/$1.log
	cp chapters/$1_edit.html chapters/$1_orig.html
	if [[ $(wc -l log/$1.log) != "0 log/$1.log" ]]
	then
		echo "Chapter ‘$title’ error log:"
		cat log/$1.log
	else
		echo "Chapter ‘$title’ quotation marks seem to be in order"
	fi
	sed -f replace_all.sc -i chapters/$1_edit.html
	echo "Done global search and replace"
	./check_common_errors.sh $1
	if [[ $(wc -l log/common_errors_$1.log) != "0 log/common_errors_$1.log" ]]
	then
		echo "Chapter ‘$title’ common errors log:"
		cat log/common_errors_$1.log
	else
		echo "Chapter ‘$title’ seems to not have common errors"
	fi
	echo "Done checking common errors"
fi
