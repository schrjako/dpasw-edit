#!/bin/sh

l=$1
r=$2
for (( i = $l; i <= $r; i++ ))
do
	rm -f log/${i}_finished.log
	./orientate_quotation_marks chapters/${i}_edit.html chapters/${i}_finished.html - 2>&1 | grep -v "Check word" >> log/${i}_finished.log
	sed -i 's/^<p[^>]*>\(.*\)[eE]nd of [cC]hapter\([^<]*\)<\/p>$/<p style="text-align: center;">\1End of Chapter\2<\/p>/' chapters/${i}_finished.html 
	if [[ "$(wc -l log/${i}_finished.log)" != "0 log/${i}_finished.log" ]]
	then
		echo "Chapter ‘${i}’ finishing error log:"
		cat log/${i}_finished.log
	fi
done
