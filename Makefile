dirs=chapters log orig_html

%:%.c
	gcc -Wall -Wextra -Wshadow -pedantic -lm -o $@ $^
%:%.cpp
	g++ -std=c++17 -Wall -Wextra -Wshadow -pedantic -lm -o $@ $^

.PHONY: clean all

all: | $(dirs)
	@echo *.c* | sed -e 's/\.cp*//g' -e 's/ /\n/g' | grep -v sub_ | xargs make > /dev/null
	$(MAKE) -C changes

$(dirs):
	mkdir $@

clean:
	@echo *.c* | sed 's/\.cp*//g' | xargs rm -fv
	@rm -fv a.out
	@rm -fv *.in
