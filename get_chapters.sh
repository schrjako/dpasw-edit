#!/bin/sh

l=$1
r=$2
for (( i = $l; i <= $r; i++ ))
do
	wget dpasw.com/chapters/${i} -O orig_html/$i.html && sleep 5 || exit
	./extract_story.sh $i
done
