#!/bin/sh

while read error
do
	if [[ $(grep "$error" chapters/$1_edit.html | wc -l) != "0" ]]
	then
		echo Check word/phrase \"$error\" in given file >> log/$1.log
	fi
done < common_errors.txt
