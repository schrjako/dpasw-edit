#include<stdio.h>
#include<string.h>
#include<errno.h>
#include<stdbool.h>
#include<ctype.h>

char line[100000], tag[1000];
char whitespace[]=" \t\n", ldq[]="“", rdq[]="”", lsq[]="‘", rsq[]="’", ndq[]="\"", nsq[]="'", dash[]="—";
char dq[3][10]={"“", "”", "\""};
char sq[3][10]={"‘", "’", "'"};

int min(int a, int b){ return a<b?a:b; }
int max(int a, int b){ return a>b?a:b; }

bool is_at(char * where, char * what){
	return strncmp(where, what, strlen(what))==0;
}

bool is_before(char * where, char * what){
	return strncmp(where-strlen(what), what, strlen(what))==0;
}

bool is_whitespace(char c){
	return strchr(whitespace, c)!=NULL;
}

bool is_not_alnum(char c){
	return !isalnum(c);
}

void write_around(FILE ** fw, int i, int dist){
	int l=max(0, i-dist), r=i+dist+1;
	for(;l<r && line[l+1];++l){
		fputc(line[l], *fw);
	}
}

int main(int argc, char ** argv){

	if(argc!=3 && argc!=4){
		fprintf(stderr, "Wrong number of arguments!\n");
		fprintf(stderr, "Usage: ./orientate_quotation_marks file_in file_out [file_err]\n");
		fprintf(stderr, "\tfile_in  - input file\n");
		fprintf(stderr, "\tfile_out - output file (overwrites existing file)\n");
		fprintf(stderr, "\tfile_err - error file (appends to existing file)\n");
		fprintf(stderr, "\t- as either file_in, file_out, or file_err means standard input, standard output, and standard error respectively.\n");
		return 1;
	}

	FILE * fr;
	if(strcmp("-", argv[1])==0) fr=stdin;
	else fr=fopen(argv[1], "r");
	if(fr==NULL){
		fprintf(stderr, "Error opening file_in {%s} for reading!\n", argv[1]);
		fputs(strerror(errno), stderr);
		return 2;
	}

	FILE * fw;
	if(strcmp("-", argv[2])==0) fw=stdout;
	else fw=fopen(argv[2], "w");
	if(fw==NULL){
		fprintf(stderr, "Error opening file_out {%s} for writing!\n", argv[2]);
		fputs(strerror(errno), stderr);
		return 3;
	}

	FILE * fe;
	if(argc==4 && strcmp(argv[3], "-")!=0) fe=fopen(argv[3], "a");
	else fe=stderr;
	if(fe==NULL){
		fprintf(stderr, "Error opening file_err {%s} for appending!\n", argv[3]);
		fputs(strerror(errno), stderr);
		return 3;
	}

	fscanf(fr, "%[^\n]", line); fgetc(fr); fgetc(fr);
	fprintf(fw, "%s\n\n", line);
	int lnum, i, j;
	bool od, os, prod=false;	//	currently opened double, single quotes, and previous row's od
	for(line[0]=' ',line[1]=0,lnum=3;fscanf(fr, "<%[^>]>%[^\n]%*c", tag, line+1)==2;line[1]=0,++lnum){
		line[strlen(line)-4]=0;
		if(od) fprintf(fe, "\t{%4d} [%s]!\n", lnum, line);
		strcpy(line+strlen(line), "\n");
		fprintf(fw, "<%s>", tag);
		od=false;
		os=false;
		for(i=1;line[i+1];++i){
			bool special=false;
			for(j=0;j<3 && !special;++j){
				if(is_at(line+i, dq[j])){
					special=true;
					if(od){	//	closing double quotation mark
						od=false;
						if(is_whitespace(line[i-1]) || !is_not_alnum(line[i+strlen(dq[j])])){
							prod=false;	//	write odd number of double quotation marks again because it would be interrupted otherwise
							fprintf(fe, "Wrong whitespaces around closing double quotation mark at line {%4d} around [", lnum);
							write_around(&fe, i, 15);
							fprintf(fe, "]!\n");
						}
						fputs(rdq, fw);
					}
					else{	//	opening double quotation mark
						od=true;
						if(!(is_whitespace(line[i-1]) || line[i-1]=='>' || is_before(line+i, dash)) || is_whitespace(line[i+strlen(dq[j])])){
							prod=false;	//	write odd number of double quotation marks again because it would be interrupted otherwise
							fprintf(fe, "Wrong whitespaces around opening double quotation mark at line {%4d} around [", lnum);
							write_around(&fe, i, 15);
							fprintf(fe, "]!\n");
						}
						fputs(ldq, fw);
					}
					i+=strlen(dq[j])-1;
				}
				else if(is_at(line+i, sq[j])){
					special=true;
					if(os){	//	maybe closing single quotation mark
						if(is_not_alnum(line[i+strlen(sq[j])])) os=false;	//	closing single quotation mark
						if(is_whitespace(line[i-1])){
							prod=false;	//	write odd number of double quotation marks again because it would be interrupted otherwise
							fprintf(fe, "Wrong whitespaces around closing single quotation mark at line {%4d} around [", lnum);
							write_around(&fe, i, 15);
							fprintf(fe, "] (should be closing, not opening)!\n");
						}
						fputs(rsq, fw);
					}
					else{	//	maybe opening single quotation mark
						if(is_whitespace(line[i-1])){	//	opening quotation mark (preceeded by whitespace)
							os=true;
							fputs(lsq, fw);
						}
						else{
							fputs(rsq, fw);
						}
					}
					i+=strlen(sq[j])-1;
				}
			}
			if(!special){
				fputc(line[i], fw);
			}
		}
		line[strlen(line)-1]=0;
		if(os){
			prod=false;	//	write odd number of double quotation marks again because it would be interrupted otherwise
			fprintf(fe, "An opening single quotation mark in line {%4d}, doesn't have a pair!\n", lnum);
		}
		if(od){
			if(prod==false){
				fprintf(fe, "Odd number of double quotation marks\n");
				fprintf(fe, "\t{%4d} [%s]!\n", lnum, line);
			}
		}
		prod=od;
		fprintf(fw, "</p>\n");
	}
	char c;
	while((c=fgetc(fr))!=EOF) fputc(c, fw);
	return 0;
}
