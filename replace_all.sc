s/\([mM]\)ind[ -]scape/\1indscape/g
s/\([dD]\)ream[ -]scape/\1reamscape/g
s/\([dD]\)inning room/\1ining room/g

s/[mM]uggle [wW]orld/Muggle World/g
s/[wW]izarding [wW]orld/Wizarding World/g
s/\([tT]\)hird floor corridor/\1hird-floor corridor/g
s/&nbsp;/ /g

s/Slytherin house/Slytherin House/g
s/[hH]ouse of [gG]ranger/House of Granger/g
s/[hH]ouse of [pP]otter/House of Potter/g
s/[hH]ouse of [bB]lack/House of Black/g
s/[hH]ouse of [gG]reengrass/House of Greengrass/g
s/[hH]ouse of [sS]lytherin/House of Slytherin/g
s/[hH]ouse of [lL]ovegood/House of Lovegood/g
s/[mM]ost [aA]ncient and [nN]oble/Most Ancient and Noble/g
s/[mM]ost [aA]ncient and [nN]oble [hH]ouse/Most Ancient and Noble House/g


s/[hH]istory of [mM]agic/History of Magic/g
s/[dD]efence [aA]gainst the [dD]ark [aA]rts/Defence Against the Dark Arts/g
s/[fF]lying [lL]essons/Flying Lessons/g
s/[hH]ealing [lL]essons/Healing Lessons/g

s/[cC]hamber of [sS]ecrets/Chamber of Secrets/g
s/[sS]hrieking [sS]hack/Shrieking Shack/g
s/[dD]epartment of [mM]ysteries/Department of Mysteries/g
s/[dD]epartment of [mM]agical [tT]rade/Department of Magical Trade/g
s/[mM]inistry of [mM]agic/Ministry of Magic/g
s/[gG]reat [aA]ccounting/Great Accounting/g
s/[iI]nternational [sS]tatute of [sS]ecrecy/International Statute of Secrecy/g
s/[sS]tatute of [sS]ecrecy/Statute of Secrecy/g
s/[dD]eathly [hH]allow/Deathly Hallow/g
s/[dD]iagon [aA]lley/Diagon Alley/g
s/[kK]nockturn [aA]lley/Knockturn Alley/g

s/[bB]lack [wW]itch [oO]f [tT]he [nN]orth/Black Witch of the North/g

