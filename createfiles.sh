#!/bin/sh

for i in {58..71}
do
	o=${i}_orig.html
	e=${i}_edit.html
	echo Chapter $i - Nature Red - Part $((i-61+5)) | tee $o > $e
	echo >> $e; echo >> $o
	echo "<p style=\"text-align: center;\">— End of Chapter $i — </p>" >> $e
	echo "<p style=\"text-align: center;\">— End of Chapter $i — </p>" >> $o
done
