#!/bin/sh

n_lines=$1
shift
fnames=$(echo chapters/*_finished.html | sed 's/ /\n/g' | sort -V)	#	chapters in correct order
grep "$1" $fnames -C $n_lines -i > search_tmp
unset $fnames
shift

while [ -n "$1" ]; do
	mv search_tmp search_tmp2
	grep -E "$1" -C $n_lines -i search_tmp2 > search_tmp
	shift
done

cat search_tmp | less
rm search_tmp search_tmp2 -f
