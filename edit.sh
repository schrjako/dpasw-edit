#!/bin/sh

a=
i=$1

while [[ "$a" == "" && -f "chapters/${i}_edit.html" ]]
do
	# The next 4 lines enable getting the wrong part of each correction to the clipboard
	# with xclip.
#	echo "Copy paste changes table and signal EOF with ctrl+d (for linux)."
#	cd changes
#	./get_changes.sh $i
#	cd ..
	#vim -o log/${i}.log chapters/${i}_edit.html
	nvim chapters/${i}_edit.html
	./finish_up.sh ${i} ${i}
	i=$((i+1))
	read a
done
