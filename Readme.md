# Dodging Prison and Stealing Witches editing

This project's aim is to quickly apply all the user-submitted corrections to
the fanfiction Dodging Prison and Stealing Witches and automate as much of the
process as possible.

## Requirements

Programs depended upon (apart from a standard shell) are:

* make
* sed
* wget
* grep
* wc
* c/c++ compiler
* xclip

## Setup

After downloading the repository set it up start make without an argument and
you're all set up:

> make

## Usage

To start you need to download chapters with get\_chapters.sh:

> ./get\_chapters.sh start end

This downloads chapters from start to end, inclusively.
To download chapters 2 (Flyuing Under the Radar - Wand) through 14 (Interlude -
Alexandra's Birthday Part) you would write the following:

> ./get\_chapters.sh 2 14

Now you can apply all the edits to chapters/*chapter\_num*\_edit.html, where 
*chapter\_num* is the number of the chapter.

log/*chapter\_num*.log should contain a description of the chapter's quotation
marks and frequently misspelled words:

- Issues with quotation marks are written out and should be human readable.
Numbers in braces ({}) denote the line number of the possible error (title is
{1}, the first paragraph {3}).
Text in brackets ([]) denotes the context - a few characters before and after
the quotation mark in question in the case of wrongly set whitespaces;
whole lines in the case of incorrectly paired quotation marks.
- For frequently misspelled words you are told to check if they are used correctly
for the chapter (if they are used in the chapter).

***IMPORTANT:***
**The log files only propose changes/point you in the right direction.
Whether or not you should take action has to be decided on a case-by-case basis.**

### Editing

To ease editing you can start editing with

> ./edit.sh chapter\_start

#### Automatic clipboard

Go to https://dodging-prison-and-stealing-witches.fandom.com/wiki/Editing\_X,
find your chapter, select from the first cell in the first row with changes to
the first cell in the last row. Copy that to the terminal and signal EOF (End
Of File; ctrl+d on linux). When you've done that you should get the next entry's
original text copied to the clipboard by calling changes/copy\_next.sh. This can
be bound to any shortcut and serves to make searching easier.

This will open a vim window with the chapter's edit and log file. After applying
all the changes and exiting vim the file will be finished up. If you want to
continue editing enter a empty line (press enter), otherwise type any character
and the editing process will finish.

Changes can be applied by the writer, using a diff-tool - e. g. vimdiff:

> vimdiff chapters/*chapter\_num*\_orig.html chapters/*chapter\_num*\_edit.html

## Inner workings

This describes the functions and behaviour of all the files.

### Makefile

Serves two purposes:

1. Makes the neccessary directories.
2. Compiles all c/c++ programs.

### get\_chapters.sh

Takes two arguments and fetches the chapters in range [first\_arg, second\_arg].
The html of each chapter is downloaded to orig\_html from dpasw.com with wget.
When the page is downloaded the story is extracted with extract\_story.sh

### extract\_story.sh

Extracts the story from the original html (stored in orig\_html). Creates 2 or 3
files - the original story, the edited story and a log.

Both story-files are in the same format:

* Title of the chapter (fist line).
* (empty line)
* 3rd -> Nth line - paragraphs in the given chapter. Each paragraph has \<p>
tags as well as tags for bold and italics.

Special characters in story-files are transformed from their html-code to their
utf-8 counterparts (`&hellip;` -> `…`; insert\_chars.sh).

Text-aligns are substituted from `align="center"` to `style="text-align: center"`.

Quotation marks are also orientated (oreintate\_quotation\_marks.c).

A global search and replace is conducted (as described under replace\_all.sc).

Common errors are checked with check\_common\_errors.sh.
