#!/bin/sh

cd "$(dirname "$0")"

cc=$(cat cc)
ce=$(cat ce)

sed -n "$ce p" ch_files/${cc}_changes.txt | sed 's/ $//g' | tr -d '\n' | xclip -i -selection clipboard

echo $((ce+1)) > ce
